# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pyson import Eval, Not, And, In, Equal
from trytond.modules.document_cost_apply_invoice.cost import (
    ApplyMethodCostMixin, ApplyMethodCostDocumentMixin,
    ApplyInvoiceAccountMixin
)


class CostType(ApplyMethodCostMixin, ApplyInvoiceAccountMixin,
        metaclass=PoolMeta):
    __name__ = 'sale.cost.type'


class CostTemplate(metaclass=PoolMeta):
    __name__ = 'sale.cost.template'

    invoice_party = fields.Many2One('party.party', 'Invoice Party',
        states={
            'invisible': ~Eval('apply_method', '').in_(
                ['invoice_in', 'invoice_out'])
        })

    def on_change_type_(self):
        super(CostTemplate, self).on_change_type_()
        if self.type_:
            self.invoice_party = self.type_.invoice_party


class SaleCost(ApplyMethodCostDocumentMixin, metaclass=PoolMeta):
    __name__ = 'sale.cost'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        readonly_description = And(
            Eval('apply_method').in_(['invoice_in', 'invoice_out']),
            Eval('invoice_lines'))
        if cls.description.states.get('readonly', None):
            cls.description.states['readonly'] |= readonly_description
        else:
            cls.description.states['readonly'] = readonly_description

        cls.invoice_lines.states['invisible'] &= Not(In(
                Eval('apply_method'), ['invoice_in', 'invoice_out']))

    def on_change_template(self):
        super(SaleCost, self).on_change_template()
        if self.template:
            self.invoice_party = self.template.invoice_party

    def on_change_type_(self):
        super(SaleCost, self).on_change_type_()
        if self.type_:
            self.invoice_party = self.type_.invoice_party

    @property
    def non_explodable(self):
        return super().non_explodable or bool(self.invoice_lines)

    def get_appliable(self, name=None):
        res = super().get_appliable(name)
        if self.apply_method in ['invoice_in', 'invoice_out']:
            res &= not bool(self.invoice_lines)
        return res

    def get_applied(self, name=None):
        return super().get_applied(name) or (
            self.apply_method in ['invoice_in', 'invoice_out']
            and bool(self.invoice_lines))

    @classmethod
    def search_applied(cls, name, clause):
        domain = super().search_applied(name, clause)

        join_operator, operator1, operator2 = 'AND', 'in', '!='
        if ((clause[1] == '!=' and clause[2])
                or (clause[1] == '=' and not clause[2])):
            join_operator, operator1, operator2 = 'OR', 'not in', '='

        domain.append([join_operator,
            ('apply_method', operator1, ['invoice_in', 'invoice_out']),
            ('invoice_lines', operator2, None)])

        return domain


class SaleCostDiscount(metaclass=PoolMeta):
    __name__ = 'sale.cost'

    def _get_invoice_lines(self, apply_method):
        lines = super()._get_invoice_lines(apply_method)
        for line in lines:
            line.discount = 0
            line.gross_unit_price = line.unit_price
        return lines


class CostType349(metaclass=PoolMeta):
    __name__ = 'sale.cost.type'

    aeat_349_ammendment = fields.Boolean('AEAT 349 ammendment',
        states={
            'invisible': Not(Equal(Eval('apply_method', ''), 'invoice_out'))
        }, depends=['apply_method'])
